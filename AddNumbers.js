﻿function add_2_numbers() {

    var n1 = $('#num1').val();
    var n2 = $('#num2').val();

    if (n1.length === 0) {

        $('#num1').attr('placeholder', "Number not entered");

    }
    if (n2.length === 0) {

        $('#num2').attr('placeholder', "Number not entered");
        $('#errors').text("Numbers not entered");
    }

    if (n1.length === 0 || n2.length === 0) {
        $('#errors').html("<span style='color: red'>Numbers not entered</span>");
    }
    else {
        $("#answer").val(parseInt(n1) + parseInt(n2));
        $('#errors').text(parseInt(n1) + parseInt(n2));
        $('#errors').css("color","green");
      
    }
}